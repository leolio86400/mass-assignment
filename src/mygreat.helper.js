

const validateNumber = (value, min, max) => {
    if (typeof value !== 'number' || Number.isNaN(value)) {
        throw new Error('wrong parameter');
    }
    if (value < min || value > max) {
        throw RangeError();
    }
    return true;
};

const validateString = (value, min, max, rgx) => {
    if (typeof value !== 'string') {
        throw new Error('wrong parameter');
    }
    if (!value.trim()) {
        throw new Error('Value is empty');
    }
    if (value.length < min || value.length > max) {
        throw Error('Value does not match requirements');
    }
    if (!!rgx && !new RegExp(rgx, 'gi').test(value)) {
        throw Error('Value does not pass validation');
    }
    return true;
};

const validateEmail = (value) => {
    return validateString(
        value,
        5,
        320,
        '^(([^<>()\\[\\]\\\\.,;:\\s@"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@"]+)*)|(".+"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$',
    );
};

const safeParseNumber = (value) => {
    if (typeof value === 'string' && value.length > 16) {
        // string larger than MAX_SAFE_INTEGER
        throw Error('String parameter too large');
    }
    const finalValue = Number(value);
    if (Number.isNaN(finalValue) || finalValue > Number.MAX_SAFE_INTEGER) {
        throw Error('Incorrect value');
    }
    return finalValue;
};

const getQueryParam = (req, key) => {
    if (!req || !req.query || !req.query.hasOwnProperty(key)) {
        throw Error('Missing parameter')
    }
    return req.query[key];
};

const getBodyParam = (req, key) => {
    if (!req || !req.body || !req.body.hasOwnProperty(key)) {
        throw Error('Missing body parameter')
    }
    return req.body[key];
};

const getBodyObject = (req) => {
    if (!req || !req.body) {
        throw Error('Missing body parameter')
    }
    if (typeof req.body !== 'object') {
        throw Error('Body not an object')
    }
    return req.body;
}

const getNumberQueryParam = (req, key) => {
    let value = getQueryParam(req, key);
    return safeParseNumber(value);
};

module.exports = {
    validateNumber,
    validateString,
    validateEmail,
    getQueryParam,
    getNumberQueryParam,
    safeParseNumber,
    getBodyParam,
    getBodyObject,
};
