'use strict';

const {
    validateString,
    validateEmail,
} = require("./mygreat.helper");

// User data model
class User {
    constructor(name, email) {
        validateEmail(email);
        validateString(name, 1, 150, '[0-9a-z\s]+');
        this.id = auth_user_id();
        this.name = name;
        this.email = email;
        this.role = 'user';
        Object.freeze(this);
    }
}

// A psudo authenticator 
// returns authenticated user ID
function auth_user_id() {
    return 2;
}


module.exports = { User, auth_user_id };
