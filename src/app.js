'use strict';

const {
    getBodyObject,
} = require("./mygreat.helper");
const { User } = require('./user');
// requirements
const express = require('express');

// constants
const PORT = process.env.PORT || 8080;

// main express program
const app = express();

// configurations
app.use(express.json());

// routes
// health check
app.get('/status', (req, res) => { res.status(200).end(); });
app.head('/status', (req, res) => { res.status(200).end(); });

app.get('/', (req, res) => {
    res.send('Post user preferences: name and email');
});

app.post('/', (req, res) => {
    try {
        const {
            email,
            name,
            ...others
        } = getBodyObject(req);
        if (email === undefined || name === undefined || Object.keys(others).length > 0) {
            res.status(400).send('Please provide both email and name');
            return;
        }
        const user = new User(name, email);
        console.log(user);
        return res.send('Your preferences have been successfully saved');
    } catch(err) {
        return res.status(400).end('Please provide both email and name' + err.toString());
    }
});

// Fix to avoid EADDRINUSE during test
if (!module.parent) {
    // HTTP listener
    app.listen(PORT, err => {
        if (err) {
            console.log(err);
            process.exit(1);
        }
        console.log('Server is listening on port: '.concat(PORT));
    });
}
// CTRL+c to come to action
process.on('SIGINT', function() {
    process.exit();
});

module.exports = app;
